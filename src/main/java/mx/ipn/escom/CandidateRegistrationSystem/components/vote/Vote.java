package mx.ipn.escom.CandidateRegistrationSystem.components.vote;

import mx.ipn.escom.CandidateRegistrationSystem.users.Candidate;
import mx.ipn.escom.CandidateRegistrationSystem.users.Voter;

public class Vote {
    private Candidate electedCandidate;
    private Voter voter;

    public Vote(Candidate electedCandidate, Voter voter) {
        this.electedCandidate = electedCandidate;
        this.voter = voter;
    }

    public Candidate getElectedCandidate() {
        return electedCandidate;
    }

    public void setElectedCandidate(Candidate electedCandidate) {
        this.electedCandidate = electedCandidate;
    }

    public Voter getVoter() {
        return voter;
    }

    public void setVoter(Voter voter) {
        this.voter = voter;
    }
}
