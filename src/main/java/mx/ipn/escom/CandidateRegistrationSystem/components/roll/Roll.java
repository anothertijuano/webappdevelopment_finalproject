package mx.ipn.escom.CandidateRegistrationSystem.components.roll;

import mx.ipn.escom.CandidateRegistrationSystem.users.Voter;

import java.util.List;

public class Roll {
    private List<Voter> electoralRoll;

    public Roll(List<Voter> electoralRoll) {
        this.electoralRoll = electoralRoll;
    }

    public List<Voter> getElectoralRoll() {
        return electoralRoll;
    }

    public void setElectoralRoll(List<Voter> electoralRoll) {
        this.electoralRoll = electoralRoll;
    }
}
