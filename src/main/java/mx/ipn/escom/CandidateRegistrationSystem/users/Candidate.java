package mx.ipn.escom.CandidateRegistrationSystem.users;

import java.util.Date;

public class Candidate extends User {
    private String position;

    public Candidate(String fistName, String lastName1, String lastName2, Date birthDate, String curp, String position) {
        super(fistName, lastName1, lastName2, birthDate, curp);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
