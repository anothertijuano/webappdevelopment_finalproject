package mx.ipn.escom.CandidateRegistrationSystem.users;

import java.util.Date;

public class Voter extends User {
    private String electoralKey;

    public Voter(String fistName, String lastName1, String lastName2, Date birthDate, String curp, String electoralKey) {
        super(fistName, lastName1, lastName2, birthDate, curp);
        this.electoralKey = electoralKey;
    }

    public String getElectoralKey() {
        return electoralKey;
    }

    public void setElectoralKey(String electoralKey) {
        this.electoralKey = electoralKey;
    }
}
