
# Candidate registration system

## Actors in the system

### Candidate

- Name
- CURP (Unique citizen code)
- Birth date
- Position

### Voter
- Name
- CURP (Unique citizen code)
- Birth date
- Electoral key

## System functionalities

### CRUD a candidate

1. Create candidate.
2. Read candidate.
3. Update candidate.
4. Delete candidate.

### Register a new electoral roll

1. Add voter to the electoral roll.

### Issue vote

1. Authenticate user by CURP.
2. Show candidates.
3. Show vote status per user.
4. Authenticate user by electoral key.
5. Register new vote.

### Show results
1. Show poll results.
